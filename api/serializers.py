from django.contrib.auth.models import User
from rest_framework import serializers
from .models import Questionario, Item, Alternativa


class UsuarioSerializer(serializers.ModelSerializer):
    password = serializers.CharField(style={'input_type': 'password'})
    email = serializers.EmailField(style={'input_type': 'email'})

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'first_name', 'last_name', 'email')


class QuestionarioSerializer(serializers.ModelSerializer):

    class Meta:
        model = Questionario
        fields = '__all__'


class ItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = Item
        fields = '__all__'


class AlternativaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Alternativa
        fields = '__all__'