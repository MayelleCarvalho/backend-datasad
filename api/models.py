from django.contrib.auth.models import User
from django.db import models


class Questionario(models.Model):

    descricao = models.CharField(max_length=150)
    autor = models.ForeignKey(User, on_delete=models.CASCADE, related_name='questionario')

    def __str__(self):
        return self.descricao


class Item(models.Model):

    descricao = models.CharField(max_length=200)
    questionario = models.ForeignKey(Questionario, on_delete=models.CASCADE, related_name='item')

    def __str__(self):
        return self.descricao


class Alternativa(models.Model):

    descricao = models.CharField(max_length=150)
    status = models.BooleanField(default=False)
    item = models.ForeignKey(Item, on_delete=models.CASCADE, related_name='alternativa')

    def __str__(self):
        return self.descricao


