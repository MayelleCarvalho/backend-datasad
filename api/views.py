from django.contrib.auth.models import User
from rest_framework import viewsets
from .models import Questionario, Item, Alternativa
from .serializers import QuestionarioSerializer, ItemSerializer, AlternativaSerializer, UsuarioSerializer


# Create your views here.

class UsuarioViewSet(viewsets.ModelViewSet):

    serializer_class = UsuarioSerializer
    queryset = User.objects.all()


class QuestionarioViewSet(viewsets.ModelViewSet):

    serializer_class = QuestionarioSerializer
    queryset = Questionario.objects.all()


class ItemViewSet(viewsets.ModelViewSet):

    serializer_class = ItemSerializer
    queryset = Item.objects.all()


class AlternativaViewSet(viewsets.ModelViewSet):

    serializer_class = AlternativaSerializer
    queryset = Alternativa.objects.all()